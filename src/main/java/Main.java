import exercice1.Cat;
import exercice1.Human;
import exercice1.Participant;
import exercice1.Robot;
import exercice2.Obstacle;
import exercice2.Treadmill;
import exercice2.Wall;

public class Main {
    public static void main(String[] args) {
        Obstacle[] obstacles = {
                new Treadmill(10),
                new Wall(150),
                new Treadmill(20)

        };

        Participant [] participants ={
                new Cat(),
                new Human(),
                new Robot()
        };
        for (Obstacle obstale:obstacles) {
            for (Participant participiant: participants) {
                if (!participiant.isActiveParticipant()) continue;
                if(!obstale.overCome(participiant)) {
                    participiant.exit();
                }

            }

        }
        System.out.println("Winers:");
        for (Participant parcipints:participants) {
            if(parcipints.isActiveParticipant()) {
                System.out.println(parcipints.getName() );
            }

        }






    }
}
