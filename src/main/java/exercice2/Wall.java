package exercice2;

import exercice1.Participant;

public class Wall implements   Obstacle {
    private final int height;

    public Wall(int height) {
        this.height = height;
    }

    @Override
    public boolean overCome(Participant participant) {
        boolean isSuccessJump=participant.getMaxJump() >= height;
        if (isSuccessJump) {
            participant.jump();
            System.out.printf("Participant %s jump wall height %d sm with success %n",
                    participant.getName(), height );
        }
        else {
            System.out.printf("Participant %s didn't jump wall height %d sm with fail %n",
                    participant.getName(), height);
        }
        return isSuccessJump;

    }
}
