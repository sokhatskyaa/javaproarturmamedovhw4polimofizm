package exercice2;

import exercice1.Participant;

public interface  Obstacle {

    boolean overCome(Participant participant);
}
