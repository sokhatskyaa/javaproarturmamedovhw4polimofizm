package exercice2;

import exercice1.Participant;

public class Treadmill implements Obstacle {
    private final int distance;

    public Treadmill(int distance) {
        this.distance = distance;
    }

    @Override
    public boolean overCome(Participant participant) {
        boolean isSuccessDistance=participant.getMaxDistance()>=distance || participant.getMaxDistance()<0;
        if(isSuccessDistance) {
            participant.run();
            System.out.printf("Participant %s ran distance %d km with success %n",
                    participant.getName(), distance);
                    }
        else {
            System.out.printf("Participant %s didn't run distance %d km with false %n",
                    participant.getName(), distance);
        }
        return isSuccessDistance;

    }
}
