package exercice0;

public class Square implements Figure {
    private double a;
    private double b;

    public Square(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double Square() {
        return a*b;
    }
}
