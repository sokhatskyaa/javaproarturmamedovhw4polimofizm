package exercice0;

public class Circle implements Figure {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    private final double PI=3.14;
    @Override
    public double Square() {

        return PI*radius*radius;
    }
}
