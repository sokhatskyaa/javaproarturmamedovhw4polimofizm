package exercice1;

public abstract class BasicParticipant implements Participant{
    boolean activeParticipant=true;
    @Override
    public boolean isActiveParticipant() {
        return activeParticipant;
    }

    @Override
    public void exit() {
        activeParticipant=false;

    }



}
