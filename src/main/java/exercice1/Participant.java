package exercice1;

public interface Participant {
//    private String name;
//    public int age;


//    public void run(String name, int age) {};
//    public void jump(String name, int age) {};

    int getMaxJump();
    int getMaxDistance();
    public void run();

    public void jump();
    String getName();
    boolean isActiveParticipant();
    void exit();

}
