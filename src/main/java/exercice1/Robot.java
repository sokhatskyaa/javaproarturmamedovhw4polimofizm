package exercice1;

public class Robot extends BasicParticipant{

    private static final int ROBOT_MAX_JUMP = 20;
    private static final int ROBOT_MAX_DISTANCE = -1;

    @Override
    public int getMaxJump() {
        return ROBOT_MAX_JUMP;
    }

    @Override
    public int getMaxDistance() {
        return ROBOT_MAX_DISTANCE;
    }

    @Override
    public void run() {
        System.out.println("Robot run");
    }

    @Override
    public void jump() {
        System.out.println("Robot jump");

    }

    @Override
    public String getName() {
        return "Robot";
    }



}
