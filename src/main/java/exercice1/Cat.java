package exercice1;

public class Cat extends BasicParticipant{
    private static final int CAT_MAX_JUMP =250 ;
    private static final int CAT_MAX_DISTANCE = 250;

    @Override
    public int getMaxJump() {
        return CAT_MAX_JUMP;
    }

    @Override
    public int getMaxDistance() {
        return CAT_MAX_DISTANCE;
    }

    @Override
    public void run() {
        System.out.println("Cat  run");
    }

    @Override
    public void jump() {
        System.out.println("Cat  jump");
    }

    @Override
    public String getName() {
        return "Cat";
    }



}
