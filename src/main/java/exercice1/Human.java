   package exercice1;

public class Human extends BasicParticipant{
    private static final int HUMAN_MAX_JUMP = 100;
    private static final int HUMAN_MAX_DISTANCE = 100;

    @Override
    public int getMaxJump() {
        return HUMAN_MAX_JUMP;
    }

    @Override
    public int getMaxDistance() {
        return HUMAN_MAX_DISTANCE;
    }

    @Override
    public void run() {
        System.out.println("Human run");
    }

    @Override
    public void jump() {
        System.out.println("Human jump");
    }

    @Override
    public String getName() {
        return "Human";
    }



}
